import React from "react";
import { Form } from "react-bootstrap";

function EventFilter(props) {
  return (
    <Form.Select aria-label="Default select example" onChange={props.onSelect}>
      <option value={-1}>Filter with Event</option>
      <option value={0}>Android</option>
      <option value={1}>ios</option>
    </Form.Select>
  );
}

EventFilter.propTypes = {};

export default EventFilter;
