import React from "react";
import { ButtonGroup, Button } from "react-bootstrap";

function Navigation({ onLeftClick, onRightClick }) {
  return (
    <div className="navigation">
      <button onClick={onLeftClick}>{"<"}</button>
      <button onClick={onRightClick}>{">"}</button>
    </div>
  );
}

Navigation.propTypes = {};

export default Navigation;
