import React from "react";
import { Form } from "react-bootstrap";

function GamesFilter(props) {
  const {} = props;

  return (
    <Form.Select aria-label="Default select example" onChange={props.onSelect}>
      <option value={-1}>Filter with Games</option>
      <option value={0}>Blast!</option>
      <option value={1}>Carz</option>
      <option value={2}>Bally Ball</option>
      <option value={3}>Screwed</option>
      <option value={4}>T-Rex</option>
    </Form.Select>
  );
}

GamesFilter.propTypes = {};

export default GamesFilter;
