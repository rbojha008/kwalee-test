import React, { Component, useEffect, useState } from "react";
import EventFilter from "../../components/EventsFilter";
import GamesFilter from "../../components/GemesFilter";
import Navigation from "../../components/Navigation";

import ChartComponent from "../Chart";
import { events, games } from "./constants";

function Main({ data }) {
  const [chartData, setChartData] = useState([]);
  const [leftIdx, setLeftIdx] = useState(0);
  const [rightIdx, setRightIdx] = useState(20);
  const [gameFilter, setGameFilter] = useState("-1");
  const [eventFilter, setEventFilter] = useState("-1");

  useEffect(() => {
    setChartData(data);
  }, [data]);

  const onLeftClick = () => {
    if (leftIdx > 0) {
      setLeftIdx(leftIdx - 1);
      setRightIdx(rightIdx - 1);
    }
  };

  const onRightClick = () => {
    if (rightIdx < chartData.length) {
      setLeftIdx(leftIdx + 1);
      setRightIdx(rightIdx + 1);
    }
  };

  const onGameSelect = (event) => {
    let val = event.target.value;
    console.log(val);

    if (val === "-1" && eventFilter === "-1") {
      setChartData(data);
    } else if (val === "-1") {
      setChartData(data.filter((v) => v["Platform"] === events[eventFilter]));
    } else if (eventFilter === "-1") {
      setChartData(data.filter((v) => v["App"] === games[val]));
    } else {
      let filteredData = data.filter((v) => v["App"] === games[val]);
      filteredData = filteredData.filter((v) => v["Platform"] === events[eventFilter]);
      setChartData(filteredData);
    }

    setGameFilter(val);
    setLeftIdx(0);
    setRightIdx(20);
  };
  const onEventSelect = (event) => {
    let val = event.target.value;

    if (val === "-1" && gameFilter === "-1") {
      setChartData(data);
    } else if (val === "-1") {
      setChartData(data.filter((v) => v["App"] === games[gameFilter]));
    } else if (gameFilter === "-1") {
      setChartData(data.filter((v) => v["Platform"] === events[val]));
    } else {
      let filteredData = data.filter((v) => v["App"] === games[gameFilter]);
      filteredData = filteredData.filter((v) => v["Platform"] === events[val]);
      setChartData(filteredData);
    }

    setEventFilter(val);
    setLeftIdx(0);
    setRightIdx(20);
  };

  console.log(chartData);

  return (
    <div>
      <div className="row">
        <div className="col-6">
          <GamesFilter onSelect={onGameSelect}></GamesFilter>
        </div>
        <div className="col-6">
          <EventFilter onSelect={onEventSelect}></EventFilter>
        </div>
      </div>
      <ChartComponent
        chartData={chartData.slice(leftIdx, rightIdx)}
        color="#3E517A"
        title={"Analytics"}
      ></ChartComponent>
      <Navigation onLeftClick={onLeftClick} onRightClick={onRightClick}></Navigation>
    </div>
  );
}

Main.propTypes = {};

export default Main;
