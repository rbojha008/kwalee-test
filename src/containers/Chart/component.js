import { Chart, registerables } from "chart.js";
Chart.register(...registerables);

import React, { Component } from "react";

function getCustomDate(val) {
  let months = ["Jan", "Feb", "Mar", "Apr", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

  let s = val.split("/");

  return `${s[0]} ${months[Number.parseInt(s[1]) - 1]} ${s[2]}`;
}

class ChartComponent extends Component {
  constructor(props) {
    super(props);
    this.chartRef = React.createRef();

    this.state = {};
  }

  componentDidMount() {
    this.myChart = new Chart(this.chartRef.current, {
      type: "line",
      options: {
        scales: {
          xAxes: [
            {
              type: "time",
              time: {
                unit: "week",
              },
            },
          ],
          yAxes: [
            {
              ticks: {
                min: 0,
              },
            },
          ],
        },
      },
      data: {
        labels: this.props.chartData.map((d) => d.time),
        datasets: [
          {
            label: this.props.title,
            data: this.props.chartData.map((d) => d.value),
            fill: "none",
            backgroundColor: this.props.color,
            pointRadius: 2,
            borderColor: this.props.color,
            borderWidth: 1,
            lineTension: 0,
          },
        ],
      },
    });
  }

  componentDidUpdate() {
    this.myChart.data.labels = this.props.chartData.map((v) => getCustomDate(v["Date"]));
    this.myChart.data.datasets[0].data = this.props.chartData.map((v) => Number.parseInt(v["Daily Users"]));
    this.myChart.update();
  }

  render() {
    return <canvas ref={this.chartRef} />;
  }
}

export default ChartComponent;
