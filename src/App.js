import React, { useEffect, useState } from "react";

import "bootstrap/dist/css/bootstrap.min.css";

import "./App.css";
import Main from "./containers/Main";

const App = () => {
  const [data, setData] = useState([]);
  useEffect(() => {
    async function fetchData() {
      const res = await fetch("http://localhost:3000/data");
      let data = await res.json();

      setData(data);
    }

    fetchData();
  }, []);

  return (
    <div className="app">
      <Main data={data}></Main>
    </div>
  );
};

export default App;
