const csv = require("csvtojson");
const path = require("path");
const fs = require("fs");

const csvFilePath = path.join(__dirname, "./data_sample.csv");
const dbJson = path.join(__dirname, "../db.json");
console.log(dbJson);

console.log(csvFilePath);
csv()
  .fromFile(csvFilePath)
  .then((jsonObj) => {
    return jsonObj;
  });

async function convert() {
  const jsonArray = await csv().fromFile(csvFilePath);
  console.log(jsonArray.length);

  let dataToBeSaved = {
    data: jsonArray,
  };

  fs.writeFile(dbJson, JSON.stringify(dataToBeSaved), () => {
    console.log("travis file updated");
  });
}

convert();
