### Install
```
npm i
```

### Run Mock Server
```
npm run start:mock
```

### Run App
```
npm run start:dev
```

### With a new Dataset (Other than the provided one)
1. Update `data_sample.csv`
2. Run below command (`this is a tool to convert csv to json and write it to DB.json, which is later used by json-server`)
```
npm run csv-json
```
3. Start Mock Server
```
npm run start:mock
```
